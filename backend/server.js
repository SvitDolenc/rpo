const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const user = require("./routes/userRoute");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, PUT");
    next();
});

app.use('/api/user', user);

const port = process.env.PORT || 3000;

app.listen(port, (err) => {
    if (err) throw new Error("Couldnt start the server!");
    console.log(`Server running on port ${port}`);
})