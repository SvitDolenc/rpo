const sql = require("../db");
const tableName = "Igralec";

let playerObject = {};

playerObject.getAll = async function() {
  return new Promise((resolve, reject) => {
    sql.query("SELECT * FROM " + tableName, (err, results) => {
      if (err) {
        reject(err);
      }
      resolve(results);
    });
  });
};

playerObject.checkIfUsernameExists = async function(username) {
  console.log("async username", username);
  return new Promise((resolve, reject) => {
    sql.query(
      "SELECT * FROM " + tableName + " WHERE uporabnisko_ime = ?",
      [username],
      (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      }
    );
  });
};

playerObject.checkIfEmailExists = async function(email) {
  return new Promise((resolve, reject) => {
    sql.query(
      "SELECT * FROM " + tableName + " WHERE mail = ?",
      [email],
      (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      }
    );
  });
};

playerObject.registerUser = async function(registrationObject) {
  return new Promise((resolve, reject) => {
    sql.query(
      "INSERT INTO " +
        tableName +
        " (mail, uporabnisko_ime, geslo) VALUES (?, ?, ?)",
      [
        registrationObject.email,
        registrationObject.username,
        registrationObject.password
      ],
      (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      }
    );
  });
};

playerObject.updateScoreAndKills = async function(id, coinsAmount) {
  return new Promise((resolve, reject) => {
    sql.query(
      "UPDATE " +
        tableName +
        " SET kills = kills + 1, coins = coins + ? WHERE idIgralec = ?",
      [
        coinsAmount,
        id
      ],
      (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      }
    );
  });
};

playerObject.updateDeaths = async function(id) {
  return new Promise((resolve, reject) => {
    sql.query(
      "UPDATE " +
        tableName +
        " SET deaths = deaths + 1 WHERE idIgralec = ?",
      [
        id
      ],
      (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      }
    );
  });
};

playerObject.getCoins = async function(id) {
  return new Promise((resolve, reject) => {
    sql.query(
      "SELECT coins from " +
        tableName +
        " WHERE idIgralec = ?",
      [
        id
      ],
      (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      }
    );
  });
};

playerObject.purchasePowerUp = async function(id, powerUpPrice) {
  return new Promise((resolve, reject) => {
    sql.query(
      "UPDATE " +
        tableName +
        " SET coins = coins - ? WHERE idIgralec = ?",
      [
        powerUpPrice,
        id
      ],
      (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      }
    );
  });
};

module.exports = playerObject;
