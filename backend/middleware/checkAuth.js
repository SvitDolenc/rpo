const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    // you recieve in format: Bearer token
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, process.env.JWT_KEY);
    next();
  } catch (err) {
    res.status(401).json({
      success: false,
      message: "Unauthorized"
    });
  }
};
