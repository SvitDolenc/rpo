const routes = require("express").Router();
const validator = require("validator");
const bcrypt = require("bcrypt");
const playerObject = require("../models/userModel");
const jwt = require("jsonwebtoken");
const checkAuth = require("../middleware/checkAuth");

routes.post("/getCoins", async (req, res) => {
  let id = req.body.id;

  try {
    let getCoinsQuery = await playerObject.getCoins(id);
    let playerCoins = getCoinsQuery[0].coins;

    return res.status(200).json({
      success: true,
      coins: playerCoins
    });
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.post("/purchasePowerUp", async (req, res) => {
  let id = req.body.id;
  let powerUpPrice = req.body.powerUpPrice;

  try {
    let getCoinsQuery = await playerObject.getCoins(id);
    let playerCoins = getCoinsQuery[0].coins;

    if (playerCoins >= powerUpPrice) {
      await playerObject.purchasePowerUp(id, powerUpPrice);

      return res.status(200).json({
        success: true,
        enoughCoins: true,
        message: "PowerUp purchased"
      });
    } else {
      return res.status(200).json({
        success: true,
        enoughCoins: false,
        message: "Not enough coins"
      });
    }
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.post("/kill", async(req, res) => {
  let id = req.body.id;
  let coins = req.body.coins;

  try {
    await playerObject.updateScoreAndKills(id, coins);
    return res.status(200).json({
      success: true
    });
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.post("/death", async(req, res) => {
  let id = req.body.id;

  try {
    await playerObject.updateDeaths(id);
    return res.status(200).json({
      success: true
    });
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.get("/getAll", async (req, res) => {

    console.log("/getAll endpoint is called!");

  try {
    let allUsers = await playerObject.getAll();

    return res.status(200).json({
      success: true,
      allUsers: allUsers
    });
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.post("/validateUsername", async (req, res) => {
  let user = req.body;

  console.log(user);

  try {
    let usernameExists = await playerObject.checkIfUsernameExists(
      user.username
    );

    console.log(usernameExists);
    if (usernameExists.length > 0) {
      return res.status(200).json({
        success: true,
        usernameExists: true
      });
    } else {
      return res.status(200).json({
        success: true,
        usernameExists: false
      });
    }
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.post("/validateEmail", async (req, res) => {
  let user = req.body;

  try {
    let emailExists = await playerObject.checkIfEmailExists(user.email);
    if (emailExists.length > 0) {
      return res.status(200).json({
        success: true,
        emailExists: true
      });
    } else {
      return res.status(200).json({
        success: true,
        emailExists: false
      });
    }
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.put("/gameLogin", async (req, res) => {
    console.log(req.body);
  const loginObject = req.body;
  try {
    // check if username exists, returns userObject if it exists
    let usernameExists = await playerObject.checkIfUsernameExists(
      loginObject.username
    );

    // get password
    // compare passwords with bcrypt function
    if (usernameExists.length > 0) {
      const match = await bcrypt.compare(
        loginObject.password,
        usernameExists[0].geslo
      );

      if (match) {
        //login
        // generate jwt token set expiration time 1 hour and send to client
        const token = jwt.sign(
          {
            username: usernameExists[0].uporabnisko_ime,
            userId: usernameExists[0].idIgralec
          },
          process.env.JWT_KEY,
          { expiresIn: "1h" }
        );
        console.log("its a match");

        res.status(200).json({
          success: true,
          validCredentials: true,
          token: token,
          timeout: 3600
        });
      } else {
        return res.status(200).json({
          success: false,
          validCredentials: false
        });
      }
    } else {
      return res.status(200).json({
        success: false,
        validCredentials: false
      });
    }
  } catch (e) {
  console.log(e);
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});


routes.post("/login", async (req, res) => {
    console.log(req.body);
  const loginObject = req.body;
  try {
    // check if username exists, returns userObject if it exists
    let usernameExists = await playerObject.checkIfUsernameExists(
      loginObject.username
    );

    // get password
    // compare passwords with bcrypt function
    if (usernameExists.length > 0) {
      const match = await bcrypt.compare(
        loginObject.password,
        usernameExists[0].geslo
      );

      if (match) {
        //login
        // generate jwt token set expiration time 1 hour and send to client
        const token = jwt.sign(
          {
            username: usernameExists[0].uporabnisko_ime,
            userId: usernameExists[0].idIgralec
          },
          process.env.JWT_KEY,
          { expiresIn: "1h" }
        );

        res.status(200).json({
          success: true,
          validCredentials: true,
          token: token,
          timeout: 3600
        });
      } else {
        return res.status(200).json({
          success: false,
          validCredentials: false
        });
      }
    } else {
      return res.status(200).json({
        success: false,
        validCredentials: false
      });
    }
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.put("/gameRegister", async (req, res) => {
  let registrationObject = req.body;
  console.log(registrationObject);

  if (!validator.isEmail(registrationObject.email)) {
    return res.status(200).json({
      success: false,
      message: "Email not valid"
    });
  } else {
    console.log("email is valid");
  }

  if (registrationObject.password != registrationObject.confirmPassword) {
    return res.status(200).json({
      success: false,
      message: "Passwords do not match"
    });
  } else {
    console.log("passwords match");
  }

  try {
    let usernameExists = await playerObject.checkIfUsernameExists(
      registrationObject.username
    );
    let emailExists = await playerObject.checkIfEmailExists(
      registrationObject.email
    );

    const returnMsg = [
      { msg: "Username already exists" },
      { msg: "Email already exists" }
    ];

    if (usernameExists.length > 0 && emailExists.length > 0) {
      return res.status(200).json({
        success: false,
        message: returnMsg[0].msg + ". " + returnMsg[1].msg
      });
    } else if (usernameExists.length > 0) {
      return res.status(200).json({
        success: false,
        message: returnMsg[0].msg
      });
    } else if (emailExists.length > 0) {
      return res.status(200).json({
        success: false,
        message: returnMsg[1].msg
      });
    }

    bcrypt.hash(registrationObject.password, 10, async (err, hash) => {
      if (err) throw err;

      registrationObject.password = hash;
      let registerUser = await playerObject.registerUser(registrationObject);

      return res.status(200).json({
        success: true,
        message: "User registered successfully"
      });
    });
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

routes.post("/register", async (req, res) => {
  let registrationObject = req.body;
  console.log(registrationObject);

  if (!validator.isEmail(registrationObject.email)) {
    return res.status(200).json({
      success: false,
      message: "Email not valid"
    });
  } else {
    console.log("email is valid");
  }

  if (registrationObject.password != registrationObject.confirmPassword) {
    return res.status(200).json({
      success: false,
      message: "Passwords do not match"
    });
  } else {
    console.log("passwords match");
  }

  try {
    let usernameExists = await playerObject.checkIfUsernameExists(
      registrationObject.username
    );
    let emailExists = await playerObject.checkIfEmailExists(
      registrationObject.email
    );

    const returnMsg = [
      { msg: "Username already exists" },
      { msg: "Email already exists" }
    ];

    if (usernameExists.length > 0 && emailExists.length > 0) {
      return res.status(200).json({
        success: false,
        message: returnMsg[0].msg + ". " + returnMsg[1].msg
      });
    } else if (usernameExists.length > 0) {
      return res.status(200).json({
        success: false,
        message: returnMsg[0].msg
      });
    } else if (emailExists.length > 0) {
      return res.status(200).json({
        success: false,
        message: returnMsg[1].msg
      });
    }

    bcrypt.hash(registrationObject.password, 10, async (err, hash) => {
      if (err) throw err;

      registrationObject.password = hash;
      let registerUser = await playerObject.registerUser(registrationObject);

      return res.status(200).json({
        success: true,
        message: "User registered successfully"
      });
    });
  } catch (e) {
    return res.status(500).json({
      success: false,
      message: e
    });
  }
});

module.exports = routes;
