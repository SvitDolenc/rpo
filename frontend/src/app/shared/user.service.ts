import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { objekt1} from '../leaderboards/body/body.component';

import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

const BACKEND_URL = environment.apiUrl + 'user/';

@Injectable()
export class UserService {
  private userAuthenticated = false;

  constructor(private http: HttpClient, private router: Router) {}

  getAuthentication() {
    return this.userAuthenticated;
  }

  setAuthentication(condition: boolean) {
    this.userAuthenticated = condition;
  }

  registerUser(registerObject) {
    return this.http.post<{ success: boolean; message: string }>(
      BACKEND_URL + 'register',
      registerObject
    );
  }

  validateUsername(username) {
    return this.http.post<{ success: boolean; usernameExists: boolean }>(
      BACKEND_URL + 'validateUsername',
      { username }
    );
  }

  validateEmail(email) {
    return this.http.post<{ success: boolean; emailExists: boolean }>(
      BACKEND_URL + 'validateEmail',
      { email }
    );
  }

  login(loginObject) {
    return this.http.post<{
      success: boolean;
      validCredentials: boolean;
      token?: string;
      timeout?: number;
    }>(BACKEND_URL + 'login', loginObject);  
  }
  
  listTable(){
    // return this.http.post<{
    //   success: boolean;
    //   allUsers: string;
    // }>(BACKEND_URL + 'getAll', listObject);
    return this.http.get<{
      success: boolean;
      allUsers: objekt1[];
    }>(BACKEND_URL + '/getAll');
  }

  logout() {
    this.deleteToken();
    this.setAuthentication(false);
    this.router.navigate(['/login']);
  }

  saveToken(token: string, timeout: Date) {
    localStorage.setItem('token', token);
    localStorage.setItem('timeout', timeout.toISOString());
  }

  deleteToken() {
    localStorage.removeItem('token');
    localStorage.removeItem('timeout');
  }

  checkIfTokenIsValid() {
    const authenticationData = this.getToken();

    if (!authenticationData) {
      this.setAuthentication(false);
      return;
    }

    const remainingTime =
      authenticationData.timeoutDate.getTime() - new Date().getTime();
    if (remainingTime > 0) {
      this.setAuthentication(true);
    } else {
      this.setAuthentication(false);
    }

    if (remainingTime > 0) {
      console.log(
        'Token is valid for: ' + Math.floor(remainingTime / 1000 / 60) + 'min'
      );
    }
  }

  getToken() {
    const token = localStorage.token;
    const timeoutDate = localStorage.timeout;
    if (!token || !timeoutDate) {
      this.setAuthentication(false);
      return;
    }

    return {
      token,
      timeoutDate: new Date(timeoutDate)
    };
  }
}
