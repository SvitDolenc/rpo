import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { UserService } from '../../shared/user.service';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

export interface objekt1 {
  idIgralec: number;
  mail: string;
  uporabnisko_ime: string;
  geslo: string;
  kills: number;
  deaths: number;
  coins: number;
  kd_ratio?: number;
  Igra_idIgra: boolean;
}

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    'idIgralec',
    'uporabnisko_ime',
    'kills',
    'deaths',
    'kd_ratio',
    'coins'
  ];
  public getAllUserDataSubscriber;
  public dataSource: MatTableDataSource<objekt1>;

  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.getAllUserDataSubscriber = this.userService
      .listTable()
      .subscribe(result => {
        if (result.success == true) {
          const data = this.addKdRatioToObjects(result.allUsers);

          this.dataSource = new MatTableDataSource(
            result.allUsers as objekt1[]
          );
          this.dataSource.sort = this.sort;
        } else if (result.success == false) {
          console.log('Podatki iz podatkovne baze niso pridobljeni!!');
        }
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addKdRatioToObjects(array) {
    for (let i = 0; i < array.length; i++) {
      let kd = parseFloat((array[i].kills / array[i].deaths).toFixed(2));
      if (isNaN(kd)) {
        kd = 0;
      }
      array[i].kd_ratio = kd;
    }

    return array;
  }

  ngOnDestroy() {
    if (this.getAllUserDataSubscriber) {
      this.getAllUserDataSubscriber.unsubscribe();
    }
  }
}
