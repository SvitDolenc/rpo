import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public wrongUsernameOrPassword = false;
  public loginSub;

  get username() {
    return this.loginForm.get('username');
  }
  get password() {
    return this.loginForm.get('password');
  }

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(4)]
      }),
      password: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(6)]
      })
    });
  }

  login() {
    if (!this.loginForm.valid) {
      console.log('Invalid form data');
      return;
    }

    const loginDataObject = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    };

    this.loginSub = this.userService
      .login(loginDataObject)
      .subscribe(result => {
        console.log(result);

        /*result.success == true
        arr = result.userAll;*/

        if (result.validCredentials) {
          this.wrongUsernameOrPassword = false;
          const now = new Date();

          console.log(now.getTime());
          const expirationDate = new Date(
            now.getTime() + result.timeout * 1000
          );
          this.userService.saveToken(result.token, expirationDate);
          this.userService.setAuthentication(true);
          this.router.navigate(['/leaderboards']);
        } else {
          this.wrongUsernameOrPassword = true;
          this.userService.setAuthentication(false);
        }
      });
  }

  ngOnDestroy() {
    if (this.loginSub) {
      this.loginSub.unsubscribe();
    }
  }
}
