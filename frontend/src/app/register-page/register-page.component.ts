import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import {
  FormGroup,
  Validators,
  AbstractControl,
  FormBuilder
} from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit, OnDestroy {
  public registrationForm: FormGroup;
  public submitted = false;
  public errorMsg = '';
  public validUsername = null;
  public validEmail = null;

  public checkUsernameSub;
  public checkEmailSub;

  get fval() {
    return this.registrationForm.controls;
  }

  constructor(
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.registrationForm = this.fb.group({
      username: [
        null,
        [Validators.required, Validators.minLength(4), Validators.maxLength(15)]
      ],
      email: [null, [Validators.required, Validators.maxLength(45)]],
      password: [null, [Validators.required, Validators.minLength(6)]],
      confirmPassword: [null, Validators.required]
    });

    this.cdr.detectChanges(); // this line is used to avoid error: Expression ___ has changed after it was checked
  }

  equalPasswords(): boolean {
    const matched: boolean =
      this.fval.password.value === this.fval.confirmPassword.value;

    if (matched) {
      this.fval.confirmPassword.setErrors(null);
    } else {
      this.fval.confirmPassword.setErrors({
        notEqual: true
      });
    }

    return matched;
  }

  checkUsername() {
    if (!this.registrationForm.controls.username.valid) {
      return;
    }

    const username = this.registrationForm.controls.username.value;

    this.checkUsernameSub = this.userService
      .validateUsername(username)
      .subscribe(result => {
        if (result.success) {
          if (result.usernameExists) {
            this.validUsername = false;
            this.fval.username.setErrors({ alreadyExists: true });
          } else {
            this.validUsername = true;
            this.fval.username.setErrors(null);
          }
        }
      });
  }

  checkEmail() {
    if (!this.registrationForm.controls.email.valid) {
      return;
    }

    const email = this.registrationForm.controls.email.value;
    this.checkEmailSub = this.userService
      .validateEmail(email)
      .subscribe(result => {
        console.log(result);
        if (result.success) {
          if (result.emailExists) {
            this.validEmail = false;
            this.fval.email.setErrors({ alreadyExists: true });
          } else {
            this.validEmail = true;
            this.fval.email.setErrors(null);
          }
        }
      });
  }

  register() {
    this.submitted = true;
    if (!this.registrationForm.valid) {
      console.log('Invalid form data');
      return;
    }

    const registerObject = {
      username: this.registrationForm.controls.username.value,
      email: this.registrationForm.controls.email.value,
      password: this.registrationForm.controls.password.value,
      confirmPassword: this.registrationForm.controls.confirmPassword.value
    };

    this.userService.registerUser(registerObject).subscribe(results => {
      console.log(results);

      // if registered successfully
      if (results.success) {
        this.router.navigate(['/login']);
      } else {
        this.errorMsg = results.message;
      }
    });

    console.log(registerObject);
  }

  ngOnDestroy() {
    if (this.checkUsernameSub) {
      this.checkUsernameSub.unsubscribe();
    }
    if (this.checkEmailSub) {
      this.checkEmailSub.unsubscribe();
    }
  }
}
